import xbmc
import urlparse
import sys

try:
        # parse the url 
        params = urlparse.parse_qs('&'.join(sys.argv[1:]))
        command = params.get('command',None)
except:
        command = None
commandList = [{'keyWord': 'activate', 'command': 'CECActivateSource'},
                {'keyWord': 'toggle', 'command': 'CECToggleState'},
                {'keyWord': 'standby', 'command': 'CECStandby'}]
# check if the command specified matches a command
for i in commandList:
        if command and command[0] == i['keyWord']:
                xbmc.executebuiltin(i['command'])

